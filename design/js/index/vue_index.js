var vm = new Vue({
	el: ".index",
	data: {

		//hoverClass: "index__image-block_closed",

		
		main_photo: "https://storge.pic2.me/upload/741/57d92f73a1e91.jpg",
		first_little_photo: "https://oboi.ws/wallpapers/17_12338_oboi_firewatch_1920x1080.jpg",
		second_little_photo: "https://oboi.ws/wallpapers/17_12338_oboi_firewatch_1920x1080.jpg",

		lifestyle: [
			{
				img: "https://lh3.googleusercontent.com/proxy/js4xPeYAbv8hGLa-SUpT686yMvf1BFhsfVnFY11Ph84jjRCbsrfehElArtT1rDI6aJmureMd1pLcQ8rOfCqG_azoQU9u_FWESwBTOnt5RJe_pVim",
				headline: "Сочетание: угги + пижама",
				description: "Для здорового сна и загородных семейных собраний"
			},
			{
				img: "https://www.navolne.life/images/201806/334091-1529853303.jpg",
				headline: "Эксклюзивные новинки 2018 года",
				description: ""
			},
			{
				img: "https://lh3.googleusercontent.com/proxy/A1utRJo5FaRhmM0hBbya2OGE7KgDqt8bVupTZskUlFI9WFJITU2Yr3r5pSbj3cNvKiigCvvC0oGTD6KM_gKpDgA0R2JN1BU",
				headline: "На базе:черные ботильоны nando muzi",
				description: "Универсальная модель в окружении основных тенденций зимы"
			},
			{
				img: "https://fashion-journal.ru/uploads/posts/2019-01/1548861611_verhnjaja-odezhda-vesna.jpg",
				headline: "Как хранить зимнюю обувь",
				description: "Используйте советы наших мастеров, чтобы следующей зимой ваша обувь вновь была готова к морозам."
			}
		],

		brandes: [
			"https://trcmoscow.ru/Upload/objects/Trc.Brand/logoBig/c1/51/95/a2/8306465_4160690.jpg",
			"https://lh3.googleusercontent.com/proxy/-DyVG6WCSHeAquUy9_qkZpb1KzxZih5jr9ymLJzK5JdFI9B-X0hq-aNq34Y_psu8hazN97bNJSrsuowLtDLhTo3O_vtkvBRTc6bMUPM9Oho1sDgI4CLcxI-nmfFi36cIZ2nivzpcig",
			"https://www.rizzicalzature.it/wp-content/uploads/2018/07/Albano-scarpe-logo.jpg",
			"https://trcmoscow.ru/Upload/objects/Trc.Brand/logoBig/c1/51/95/a2/8306465_4160690.jpg",
			"https://cleopatra.od.ua/wp-content/uploads/2019/01/Angelo-Giannini.jpg",
			"https://www.baldan.ind.br/imagens/placeholder?300x300",
			"https://cdn.worldvectorlogo.com/logos/baldinini.svg",
			"https://redoycorp.com/wp-content/uploads/2018/09/BARRACUDA-LOGO.jpeg"
		],

		instagram_fc: [
			{
				url: "./design/assets/images/instagram-photo.png",
				image: 'url(./design/assets/images/instagram-photo.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "./design/assets/images/instagram-photo1.png",
				image: 'url(./design/assets/images/instagram-photo1.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "./design/assets/images/instagram-photo1.png",
				image: 'url(./design/assets/images/instagram-photo1.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "./design/assets/images/instagram-photo1.png",
				image: 'url(./design/assets/images/instagram-photo1.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			}
		],

		instagram_sc: [
			{
				url: "./design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "./design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "./design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "./design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			}
		]

	}
})