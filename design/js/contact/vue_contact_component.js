var vm = new Vue({
	el: ".contact",
	data: {
		activeObject: {},
		shops: [
			{
				id:0,
				name: "центральный",
				address: "Кооператор Дона, Ворошиловский пр. 91/1",
				number: "8 800 256 10 86",
				mapSrc: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2709.241465320887!2d39.712284015043615!3d47.23142252181352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40e3b9a811802d3b%3A0x7f0fb462fab31c2!2sALBANA!5e0!3m2!1sru!2sru!4v1584132107926!5m2!1sru!2sru",
				requisites: [
					{
						item: "Адрес1",
						requisite: "344082, г.Ростов-на-Дону, ул. Обороны, 1, кв.12"
					},
					{
						item: "ИНН",
						requisite: "6165436363434"
					},
					{
						item: "ОГРНИП",
						requisite: "12541243525345345"
					},
					{
						item: "Р/С",
						requisite: "5234534364564645645 в филиале 'Ростовский' ОАО 'Альфа-банк'"
					},
					{
						item: "БИК",
						requisite: "025235354"
					},
					{
						item: "К/С",
						requisite: "353523534534363463"
					}
				],
				style: {
					backgroundColor: "#f4f4f4"
				}				
			},
			{
				id:1,
				name: "чехова",
				address: "Максима Горького, 150/63",
				number: "8 863 221 83 83",
				mapSrc: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d402.75766875911967!2d39.72073782915212!3d47.22793308053466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40e3b9a754f9aef5%3A0x75a79b3f1e69cd81!2z0YPQuy4g0JzQsNC60YHQuNC80LAg0JPQvtGA0YzQutC-0LPQviwgMTUwLCDQoNC-0YHRgtC-0LIt0L3QsC3QlNC-0L3Rgywg0KDQvtGB0YLQvtCy0YHQutCw0Y8g0L7QsdC7LiwgMzQ0MDAw!5e0!3m2!1sru!2sru!4v1584217509499!5m2!1sru!2sru",
				requisites: [
					{
						item: "Адрес2",
						requisite: "344082, г.Ростов-на-Дону, ул. Обороны, 1, кв.12"
					},
					{
						item: "ИНН",
						requisite: "6165436363434"
					},
					{
						item: "ОГРНИП",
						requisite: "12541243525345345"
					},
					{
						item: "Р/С",
						requisite: "5234534364564645645 в филиале 'Ростовский' ОАО 'Альфа-банк'"
					},
					{
						item: "БИК",
						requisite: "025235354"
					},
					{
						item: "К/С",
						requisite: "353523534534363463"
					}
				],
				style: {
					backgroundColor: "#fff"
				}					
			},
			{
				id:2,
				name: "мегамаг",
				address: "Пойменная улица, 1",
				number: "8 863 221 83 83",
				mapSrc: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1139.689132489426!2d39.723163443972105!3d47.20380687171717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40e3b9613762d3a9%3A0x55ae96c7b443c8ab!2z0J_QvtC50LzQtdC90L3QsNGPINGD0LsuLCAxLCDQoNC-0YHRgtC-0LIt0L3QsC3QlNC-0L3Rgywg0KDQvtGB0YLQvtCy0YHQutCw0Y8g0L7QsdC7LiwgMzQ0MDAy!5e0!3m2!1sru!2sru!4v1584217693162!5m2!1sru!2sru",
				requisites: [
					{
						item: "Адрес3",
						requisite: "344082, г.Ростов-на-Дону, ул. Обороны, 1, кв.12"
					},
					{
						item: "ИНН",
						requisite: "6165436363434"
					},
					{
						item: "ОГРНИП",
						requisite: "12541243525345345"
					},
					{
						item: "Р/С",
						requisite: "5234534364564645645 в филиале 'Ростовский' ОАО 'Альфа-банк'"
					},
					{
						item: "БИК",
						requisite: "025235354"
					},
					{
						item: "К/С",
						requisite: "353523534534363463"
					}
				],
				style: {
					backgroundColor: "#fff"
				}					
			}
		]

		

	}, 
	methods: {
		change: function() {
			this.activeObject = this.shops[event.target.dataset.id];
			for(item of this.shops) {
				item.style.backgroundColor = "#fff";
			}
			this.shops[event.target.dataset.id].style.backgroundColor = "#f4f4f4";
			
		},

		MapCheck: function() {
			console.log(100);
		}

	},
	created: function() {
		this.activeObject = this.shops[0];
		// axios.get("")
		// 	.then(function(responce) {

		// 	})
	}
})