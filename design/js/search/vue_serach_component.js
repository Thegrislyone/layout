var vm = new Vue({
	el:".search",
	data: {
		menu: {
			season: {
				isOpen: true,

				list: [
					{id:0, text:"Летняя"},
					{id:1, text:"Зимняя"},
					{id:2, text:"Демисезонная"},
					{id:3, text:"Всесезонная"}
				]

			},

			category: {
				isOpen: true,
				list: [
					"Балетки",
					"Босоножки",
					"Ботильоны",
					"Ботинки",
					"Ботфорты",
					"Кеды",
					"Кроссовки",
					"Мокасины",
					"Полуботинки",
					"Полусапожки",
					"Резиновые ботинки",
					"Сабо",
					"Сандалии",
					"Сапоги",
					"Сланцы",
					"Слипоны",
					"Сникерсы",
					"Тапочки",
					"Топсайдеры",
					"Туфли",
					"Шлёпанцы",
					"Эспадрильи",
					"Угги"
				]
			},
			brandes: {
				isOpen: true,
				list: [
					"Angelo Giannini",
					"Baldinini",
					"Giovanni Fabiani",
					"Lancaster",
					"Laura Bellariva",
					"Loriblu",
					"Mara",
					"Voile Blanche"
				]
			},
			size: {
				isOpen:true,
				list: [
					{id:0, presence:true, size: 35},
					{id:1, presence:false, size: 36},
					{id:2, presence:true, size: 37},
					{id:3, presence:true, size: 38},
					{id:4, presence:false, size: 39},
					{id:5, presence:true, size: 35.5},
					{id:6, presence:true, size: 36.5},
					{id:7, presence:true, size: 37.5},
					{id:8, presence:false, size: 38.5},
					{id:9, presence:true, size: 39.5}
				]
			},

			catalog_blocks: [
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: true,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					],
					season: [
						"Зимняя",
						"Демисезонная"
					],
					category: "Сапоги",
					brand: "Angelo Giannini"
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: true,
					actionCost: 19990,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				}
			],

			catalog: {
				popularFilter: true,
				newFilter: false,
				expensiveFilter: false,
				cheepFilter: false
			},

			quantity: 256,

			instagram: {
				url: "https://i.pinimg.com/474x/11/94/9a/11949add2687e05b22c3d8256628e708.jpg",
				likes: 162,
				comments: 25
			},
			hashtages: [
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки",
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки",
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки",
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки"
			]
			
		}
		
	},
	methods: {
		filter: function(event) {
			console.log(event.target);
		}
	},
	created: function() {

	}
})