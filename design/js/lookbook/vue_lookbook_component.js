Vue.component('lookbook-component',{
	data: function() {
		return {

		}
	},

	props: ['item'],

	template: `
		<div class="lookbook__commponent">
			<div class="lookbook__first-layer">
				<div class="lookbook__first-layer-img-block">
					<img :src="item.firsLayer.img_src" class="lookbook__first-layer-img">
				</div>
				<div class="lookbook__article-block lookbook__first-layer-article-block">
					<span class="lookbook__article-type">{{ item.firsLayer.text_block.dir }}</span>
					<h4 class="lookbook__article-name">{{ item.firsLayer.text_block.headline }}</h4>
					<span class="lookbook__article-text">{{ item.firsLayer.text_block.text }}</span>
				</div>
			</div>
			<div class="lookbook__second-layer">
				<div class="lookbook__sl-first-block">
					<span class="lookbook__article-type">{{ item.secondLayer.text_block.dir }}</span>
					<h4 class="lookbook__article-name">{{ item.secondLayer.text_block.headline }}</h4>
					<span class="lookbook__article-text">{{ item.secondLayer.text_block.text }}</span>
				</div>
				<div class="lookbook__sl-second-block">
					<img :src="item.secondLayer.img_src">
				</div>
				<div class="lookbook__sl-third-block">
					<div>
						<img :src="item.secondLayer.text_img_block.img_src">
						<div>
							<span class="lookbook__article-type">{{ item.secondLayer.text_img_block.dir }}</span>
							<h4 class="lookbook__article-name">{{ item.secondLayer.text_img_block.headline }}</h4>
							<span class="lookbook__article-text">{{ item.secondLayer.text_img_block.text }}</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lookbook__third-layer" v-if="item.thirdLayer.isExist">
				<img :src="item.thirdLayer.img_src" class="lookbook__third-layer-absolute">
				<img :src="item.thirdLayer.img_src" style="opacity: 0;width: 100%;margin-left: auto;">
				<span class="lookbook__third-layer-type">{{ item.thirdLayer.dir }}</span>
				<h3 class="lookbook__third-layer-headline">{{ item.thirdLayer.headline }}</h3>
				
			</div>
		</div>
	`


})


var vm = new Vue({
	el: ".lookbook",
	data: {
		components: [
			{
				firsLayer: {
					img_src: "../../design/assets/images/lookbook/firstLayer1.png",
					text_block: {
						dir: "хотелки",
						headline: "Все свое ношу с собой",
						text: "12 праздничных клатчей для новогоднего настроения."
					}
				},
				secondLayer: {
					text_block: {
						dir: "хотелки",
						headline: "Лист покупок: мужская обувь для гололеда",
						text: "Обувь, которая сохранит осанку прямой, а походку уверенной в условиях снежно-мокрой зимы."
					},
					img_src: "../../design/assets/images/lookbook/secondLayer1.png",
					text_img_block: {
						img_src: "../../design/assets/images/lookbook/secondLayer2.png",
						dir: "модные истории",
						headline: "Больше цвета вместо психотерапевта",
						text: "Яркие краски - лучшее лекарство от затяжной зимней хандры.."

					}
				},
				thirdLayer: {
					isExist: true,
					img_src:"../../design/assets/images/textpagePhoto.png",
					text: {
						dir: "Мировая коллекция",
						headline:"Семейные ценности: что сделал Джанни Версаче для нас"
					}
				}
			},
			{
				firsLayer: {
					img_src: "../../design/assets/images/lookbook/firstLayer2.png",
					text_block: {
						dir: "модный словарь",
						headline: "Валенки, валенки, не подшиты стареньки",
						text: "Вовсе не из России, как многие думают."
					}
				},
				secondLayer: {
					text_block: {
						dir: "хотелки",
						headline: "На базе: черные ботильоны Nando Muzi",
						text: "Универсальная модель в окружении основных тенденций зимы"
					},
					img_src: "../../design/assets/images/lookbook/secondLayer11.png",
					text_img_block: {
						img_src: "../../design/assets/images/lookbook/secondLayer22.png",
						dir: "гардероб",
						headline: "Сочетание: угги + пижама",
						text: "Для здорового сна и загородных семейных собраний"

					}
				},
				thirdLayer: {
					isExist: true,
					img_src:"../../design/assets/images/textpagePhoto.png",
					text: {
						dir: "Мировая коллекция",
						headline: "Семейные ценности: что сделал Джанни Версаче для нас"
					}
				}
			},
			{
				firsLayer: {
					img_src: "../../design/assets/images/lookbook/firstLayer2.png",
					text_block: {
						dir: "модный словарь",
						headline: "Валенки, валенки, не подшиты стареньки",
						text: "Вовсе не из России, как многие думают."
					}
				},
				secondLayer: {
					text_block: {
						dir: "хотелки",
						headline: "На базе: черные ботильоны Nando Muzi",
						text: "Универсальная модель в окружении основных тенденций зимы"
					},
					img_src: "../../design/assets/images/lookbook/secondLayer11.png",
					text_img_block: {
						img_src: "../../design/assets/images/lookbook/secondLayer22.png",
						dir: "гардероб",
						headline: "Сочетание: угги + пижама",
						text: "Для здорового сна и загородных семейных собраний"

					}
				},
				thirdLayer: {
					isExist: false,
					img_src:"../../design/assets/images/textpagePhoto.png",
					text: {
						dir: "Мировая коллекция",
						headline: "Семейные ценности: что сделал Джанни Версаче для нас"
					}
				}
			}
		],

		instagram: {
			img: "../../design/assets/images/instagram-photo1.png",
			likes: 125,
			comments: 57
		}

	},
	methods: {

	},

	created: function() {

	}

})