var vm = new Vue({
	el: ".account",
	data: {
		def: {

			isLogin: true,

			cabinet: [
				{ text: "Моя информация", img: "" },
				{ text: "Смена пароля", img: "" },
				{ text: "Закладки", img: "" },
				{ text: "История заказов", img: "../../design/assets/images/arrow.png" },
				{ text: "Бонусные баллы", img: "" },
				{ text: "Подписка на скидки", img: "" },
				{ text: "Выход", img: "" }
			],

			orders: [
				{
					id:0,
					orderNumber: 2324,
					executed: false,
					date: "12.04.2018",
					items_quantity: 0,
					total: 29999,
					urgentDelivery: true,
					items: [
						{item: "fff",quantity:1, cost: 154},
						{item: "www", quantity:2, cost: 2521}
					],
					changes: [
						{date: 15, status: "sdfs"}
					],
					comment: `Привет! Твой заказ №2324 выполнен. Тебе была вручена карта, по которой ты можешь получить 12% скидки на продукцию интернет-магазина :) 
Карта будет закреплена за твоим аккаунтом на сайте и зайдя на сайт под своим email и паролем, сможешь видеть сразу свои специальные цены с учетом скидки.
-- 
С увлажнением `
				},
				{
					id:1,
					orderNumber: 11,
					executed: true,
					date: "02.02.2018",
					items_quantity: 0,
					total: 7000,
					urgentDelivery: true,
					items: [
						{item: "fff",quantity:1, cost: 154},
						{item: "www", quantity:2, cost: 2521}
					],
					changes: [
						{date: 15, status: "sdfs"}
					],
					comment: `Привет! Твой заказ №2324 выполнен. Тебе была вручена карта, по которой ты можешь получить 12% скидки на продукцию интернет-магазина :) 
Карта будет закреплена за твоим аккаунтом на сайте и зайдя на сайт под своим email и паролем, сможешь видеть сразу свои специальные цены с учетом скидки.
-- 
С увлажнением `
				},
				{
					id:2,
					orderNumber: 122,
					executed: true,
					date: "22.03.2018",
					items_quantity: 0,
					total: 5000,
					urgentDelivery: true,
					items: [
						{item: "fff",quantity: 3, cost: 1541},
						{item: "www", quantity:2, cost: 2521}
					],				
						
					changes: [
						{date: 15, status: "sdfs"}
					],
					comment: `Привет! Твой заказ №2324 выполнен. Тебе была вручена карта, по которой ты можешь получить 12% скидки на продукцию интернет-магазина :) 
Карта будет закреплена за твоим аккаунтом на сайте и зайдя на сайт под своим email и паролем, сможешь видеть сразу свои специальные цены с учетом скидки.
-- 
С увлажнением `
				}
			],
		},
		

		activeOrder: {},

		// orderCondition: "account__order-wrapper-closed"


	},

	methods: {
		orderOpen: function() {
			this.activeOrder = this.def.orders[event.target.dataset.id];
		}
	},

	created: function() {
		for(item of this.def.orders) {
			for(value of item.items) {
				item.items_quantity += value.quantity;
			}
		}
	}


})