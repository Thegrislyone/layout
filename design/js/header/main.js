let burger = document.querySelector(".header__burger"),

	first_strip = burger.children[0],
	second_strip = burger.children[1],
	third_strip = burger.children[2];


$(".header__main-menu-button").mouseenter(() => {

	$(".header__main-menu-button").stop().animate({backgroundColor:"#b6814f"},350)

	setTimeout(function(){
		$(".header__dotted-burger").fadeIn(0);
	}, 200);

	dynamics.animate(first_strip, {
		translateX: -35,
		scale: 1,
  	
		}, {
		 type: dynamics.gravity,
		  bounciness:0,
		  elasticity:1000,
		  duration: 100
	})

	dynamics.animate(second_strip, {
		translateX: 35,
		scale: 1,
 
		}, {
		  type: dynamics.gravity,
		  bounciness:0,
		  elasticity:1000,
		  duration: 100
	})

	dynamics.animate(third_strip, {
		translateX: -35,
		scale: 1,
 
		}, {
		  type: dynamics.gravity,
		  bounciness:0,
		  elasticity:1000,
		  duration: 100
	})

})

$(".header__main-menu-button").mouseleave(() => {

	$(".header__main-menu-button").stop().animate({backgroundColor:"#fff"},350)

	setTimeout(function(){
		$(".header__dotted-burger").fadeOut(0);
	}, 200);

	dynamics.animate(first_strip, {
		translateX: 0,
		scale: 1,
 
		}, {
		  type: dynamics.spring,
		  frequency: 800,
		  friction: 300,
		  duration: 2500,
		  anticipationSize: 100,
		  anticipationStrength: 500
	})

	dynamics.animate(second_strip, {
		translateX: 0,
		scale: 1,	
 
		}, {
		  type: dynamics.spring,
		  frequency: 800,
		  friction: 300,
		  duration: 2500,
		  anticipationSize: 100,
		  anticipationStrength: 500
	})

	dynamics.animate(third_strip, {
		translateX: 0,
		scale: 1,
 
		}, {
		  type: dynamics.spring,
		  frequency: 800,
		  friction: 300,
		  duration: 2500,
		  anticipationSize: 100,
		  anticipationStrength: 500
	})

})

// function cart_mouseover() {

// 	setTimeout(
// 		function() {
// 			$(".header__cart-image").css('filter','invert(100%)');

// 		},50);

	

	

// 	$(".header__cart-image").stop().animate({filter:"invert(100%)"},350);

// 	$(".header__cart-element").stop().animate({backgroundColor:"#b6814f"},350);
// 	$(".header__order-number").stop().animate({color:"#fff"},350);

	
// }

// function cart_mouseleave(){

// 	setTimeout(
// 		function() {
// 			$(".header__cart-image").css('filter','invert(0%)');

// 		},150);

// 	$(".header__cart-element").stop().animate({backgroundColor:"#ffffff"},350);
// 	$(".header__order-number").stop().animate({color:"#000"},350);
// }


$(window).scroll(header_movement);

header_movement();

function header_movement() {
	if ($(window).scrollTop() > 125) {
		$(".header__category-tabs").animate({

		}, 60);
		$(".header__gender-category-tabs").animate({
			height: "0px",
			color: "transparent"
		}, 60);
		$(".header__logo-element").animate({
			height: "68px"
		}, 60);
		$(".search-submenu__search").animate({
			top: "93px"
		}, 60);
		$(".header__first-hat-layer").animate({
			gridTemplateRows: "93px"
		}, 60);
		$(".header__shoe-submenu").animate({
			top: "153px"
		}, 60);

		$(".header__bugs-submenu").animate({
			top: "153px"
		}, 60);
		$(".header__accessories-submenu").animate({
			top: "153px"
		}, 60);
	} 
	else {
		$(".header__category-tabs").animate({

		}, 60);
		$(".header__gender-category-tabs").animate({
			height: "60px",
			color: "000"
		}, 60);
		$(".header__logo-element").animate({
			height: "125px"
		}, 60);
		$(".search-submenu__search").animate({
			top: "185px"
		}, 60);
		$(".header__first-hat-layer").animate({
			gridTemplateRows: "125px"
		}, 60);
		$(".header__shoe-submenu").animate({
			top: "244px"
		}, 60);

		$(".header__bugs-submenu").animate({
			top: "244px"
		}, 60);
		$(".header__accessories-submenu").animate({
			top: "244px"
		}, 60);
	}
}




$(".header__shoes").mouseenter(()=>{
	$(".header__shoe-submenu").stop().fadeIn(350);
})

$(".header__shoes").mouseleave(()=>{
	$(".header__shoe-submenu").stop().fadeOut(350);
})




$(".header__bugs-element").mouseenter(()=>{
	$(".header__bugs-submenu").stop().fadeIn(350);
})

$(".header__bugs-element").mouseleave(()=>{
	$(".header__bugs-submenu").stop().fadeOut(350);
})

$(".header__accessories-element").mouseenter(()=>{
	$(".header__accessories-submenu").stop().fadeIn(350);
})

$(".header__accessories-element").mouseleave(()=>{
	$(".header__accessories-submenu").stop().fadeOut(350);
})


$(".header__main-menu-button").click(() => {
	$(".main-menu").fadeIn(350);
	// $(".header").css('height','100%');
	// $(".header__logo-element").css('zIndex','6');
	$(".header__under-logo-city-wrapper").css('display','none');
	$(".header__full-logo").css('filter','invert(100%)');
})

$(".main-menu__close-icon").click(() => {
	$(".main-menu").fadeOut(350);
	// $(".header").css('height','245px');
	$("body").css('height','100%');
	$(".header__logo-element").css('zIndex','0');
	$(".header__under-logo-city-wrapper").css('display','block');
	$(".header__full-logo").css('filter','invert(0%)');
})


$(".header__city-element").click(()=>{
	$(".city-menu").fadeIn(350);

	// $(".header__logo-element").css('zIndex','6');
	$(".header__city-element").css('zIndex','70');

	$(".header__under-logo-city-wrapper").css('display','none');
})
$(".header__under-logo-city-wrapper").click(()=>{
	$(".city-menu").fadeIn(350);

	// $(".header__logo-element").css('zIndex','6');
	$(".header__under-logo-city-wrapper").css('zIndex','70');

})
$(".city-menu__close-icon").click(()=>{
	$(".city-menu").fadeOut(350);
	// setTimeout(function() {
	// 	// $(".header__logo-element").css('zIndex','0');
	// },350)
	
	$(".header__under-logo-city-wrapper").css('display','block');

	// $(".header__logo-element").css('zIndex','0');
	$(".header__city-element").css('zIndex','50');
})


$(".header__account-element").click(() => {
	$(".account-menu").fadeIn(350);
	// $(".header__logo-element").css('zIndex','8');
	$(".header__account-element").css('zIndex','70');
	
})

$(".account-menu__close-icon").click(() => {
	$(".account-menu").fadeOut(350);
	// setTimeout(function() {
	// 	$(".header__logo-element").css('zIndex','0')
	// },350)
	$(".header__account-element").css('zIndex','50');
})




$(".header__cart-element").click(()=>{
	$(".cart-menu").fadeIn(350);
}) 


	

 


$(".header__cart-close-icon").click(()=> {


	$(".cart-menu").fadeOut(350);

return false;
})



