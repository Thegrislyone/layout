var vm = new Vue({
	el: ".header__category-tabs",
	data: {
		shoe_submenu: {
			conditionClass: 'header__shoe-submenu-closed',
			kategory: [
				{id:0,word:"Зимняя"},
				{id:1,word:"Демисезонная"},
				{id:2,word:"Летняя"}
			],
			first_column: [
				{id:0,word:"Балетки"},
				{id:1,word:"Босоножки"},
				{id:2,word:"Ботильоны"},
				{id:3,word:"Ботинки"},
				{id:4,word:"Ботфорты"},
				{id:5,word:"Кеды"},
				{id:6,word:"Кроссовки"},
				{id:7,word:"Мокасины"}
			],
			second_column: [
				{id:0,word:"Полуботинки"},
				{id:1,word:"Полусапожки"},
				{id:2,word:"Сабо"},
				{id:3,word:"Сандалии"},
				{id:4,word:"Сапоги"},
				{id:5,word:"Сланцы"},
				{id:6,word:"Слипоны"}
			],
			third_column: [
				{id:0,word:"Сникерсы"},
				{id:1,word:"Тапочки"},
				{id:2,word:"Топсайдеры"},
				{id:3,word:"Туфли"},
				{id:4,word:"Угги"},
				{id:5,word:"Шлёпанцы"},
				{id:6,word:"Эспадрильи"}
			],
			brandes: [
				{id:0,word:"Angelo Giannini"},
				{id:1,word:"Baldinini"},
				{id:2,word:"Giovanni Fabiani"},
				{id:3,word:"Lancaster"},
				{id:4,word:"Laura Bellariva"},
				{id:5,word:"Loriblu"},
				{id:6,word:"Mara"},
				{id:7,word:"Voile Blanche"}
			]

		},

		bugs_submenu: {
			conditionClass: 'header__bugs-submenu-closed',
		},
		accessories_submenu: {
			conditionClass: 'header__accessories-submenu-closed',
		}
		
	}
})