var vm = new Vue({
	el: ".about-us",
	data: {
		main_images: {
			main_src: "../../design/assets/images/about_main-image.png",
			sub_image_1: "../../design/assets/images/about_sub-img.png",
			sub_image_2: "../../design/assets/images/about_sub-img.png",
			sub_image_3: "../../design/assets/images/about_sub-img.png"
		},
		
		first_slider_images: [
			{ src:"../../design/assets/images/about_sub-img.png", description:"" },
			{ src:"../../design/assets/images/first_slider_photo2.png", description:"" },
			{ src:"../../design/assets/images/first_slider_photo1.png", description:"" },
			{ src:"../../design/assets/images/about_sub-img.png", description:"" },
			{ src:"../../design/assets/images/first_slider_photo2.png", description:"" },
			{ src:"../../design/assets/images/about_sub-img.png", description:"" },
			{ src:"../../design/assets/images/first_slider_photo1.png", description:"" }
		],

		first_block_slider: [
			{ src: "../../design/assets/images/first_article_slider.png", description:"" },
			{ src: "../../design/assets/images/second_article_slider.png", description:"" },
			{ src: "../../design/assets/images/first_article_slider.png", description:"" },
			{ src: "../../design/assets/images/second_article_slider.png", description:"" }
		],
		second_block_slider: [
			{ src: "../../design/assets/images/second_article_slider.png", description:"" },
			{ src: "../../design/assets/images/first_article_slider.png", description:"" },
			{ src: "../../design/assets/images/second_article_slider.png", description:"" },
			{ src: "../../design/assets/images/first_article_slider.png", description:"" }
		],
		
		instagram_fc: [
			{
				url: "../../design/assets/images/instagram-photo.png",
				image: 'url(../../design/assets/images/instagram-photo.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "../../design/assets/images/instagram-photo1.png",
				image: 'url(../../design/assets/images/instagram-photo1.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "../../design/assets/images/instagram-photo1.png",
				image: 'url(../../design/assets/images/instagram-photo1.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "../../design/assets/images/instagram-photo1.png",
				image: 'url(../../design/assets/images/instagram-photo1.png)',
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			}
		],

		instagram_sc: [
			{
				url: "../../design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "../../design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "../../design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			},
			{
				url: "../../design/assets/images/instagram-photo1.png",
				likes: 103,
				comments: 15,
				hoverClass: "index__image-block_closed"
			}
		]

	},
	methods: {

	}
})