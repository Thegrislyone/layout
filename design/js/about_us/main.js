$(document).ready(function(){


  	$('.about-us__slider').slick({
	    dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true
  	});


   	$('.about-us__image-block-slider').slick({
	    dots: true,
	    arrows:false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: false,
		variableWidth: false
  	});


});
