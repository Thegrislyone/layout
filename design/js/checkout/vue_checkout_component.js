Vue.component('cart-component', {
	data: function() {
		return {
			
		}
	},

	props: ['item'],


	template: `
		<div class="checkout__item-element">
			<div class="checkout__item-img-block">
				<img :src="item.img_src" class="checkout__item-img">
			</div>

			<div class="checkout__description-block">
				<span class="checkout__item-name">{{ item.item_name }}</span>
				<span class="checkout__item-color">{{ item.iten_color }}</span>
				<span class="checkout__item-size">{{ item.item_size + " размер" }}</span>
				<div class="checkout__item-q-and-c">
					<div class="checkout__item-cost-block">
						<span class="checkout__item-cost">{{ item.item_price + " Р" }}</span>
						<img src="../../design/assets/images/little_close_icon.png">
					</div>
					<div class="checkout__item-quantity-block">
						<input type="number" class="checkout__item-quantity-field" :value="item.quantity">
						<span class="checkout__item-equal-sign">=</span>
					</div>
					<div class="checkout__item-final-cost-block">
						<span class="checkout__item-final-cost">{{ item.item_price * item.quantity + " Р" }}</span>
					</div>
				</div>
			</div>

		</div>
	`,

	methods: {
		recount: function() {

		}
	},

	created: function() {
		
	}
});


var vm = new Vue({
	el: ".checkbox__item-bar",
	data: {
		items: [
				{
					id:0,
					img_src: "../../design/assets/images/shoe_photo.png",
					item_name:"Туфли CALVIN KLEIN",
					item_color:"TWIGGY, чёрный",
					item_size: 37,
					item_price: 9890,
					quantity: 2
				},
				{
					id:1,
					img_src: "../../design/assets/images/bag_photo.png",
					item_name:"Сумка MARC JACOBS THE GRIND",
					item_color:"черный",
					item_size: "",
					item_price: 30290,
					quantity: 1
				},
				{
					id:2,
					img_src: "https://www.ecco-shoes.ru/images/eshop/img/jpg/bigw/833614_59236.jpg",
					item_name:"Ботинки какие-то",
					item_color:"коричневый",
					item_size: 42,
					item_price: 3500,
					quantity: 5
				}
			]
	},
	methods: {

	}
});
