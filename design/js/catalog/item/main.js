$(document).ready(function(){

 $('.item__main-img-wrapper').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.item__vert-slider'
});
$('.item__vert-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.item__main-img-wrapper',
  dots: false,
  centerMode: false,
  focusOnSelect: true,
  vertical:true
});

$('.item__similiar-wrapper').slick({
  slidesToShow: 3,
  slidesToScroll: 3,
  dots: true,
  arrows: false
});




});
