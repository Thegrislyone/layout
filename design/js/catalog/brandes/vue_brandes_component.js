Vue.component('brand',{
	data: function() {
		return {

		}
	},

	props: ['src'],

	template: `
		<div>
			<img :src="src" class="brandes__img">
			<img src="../../../design/assets/images/brandes/brand1hover.png" class="brandes__img-hover">
		</div>
	`

});


var vm = new Vue({
	el: ".brandes",
	data: {
		brandes: [
			{id:0, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:1, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:2, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:3, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:4, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:5, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:6, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:7, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:8, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:9, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:10, src: "../../../design/assets/images/brandes/brand1.png"},
			{id:11, src: "../../../design/assets/images/brandes/brand1.png"}
		]
	},

	methods: {

	}
});
