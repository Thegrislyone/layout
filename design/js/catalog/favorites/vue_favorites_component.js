Vue.component('product-block', {
	data: function() {
		return {

		}
	},

	props: ['item'],

	template: `
		<div class="favorites__product">
			<div class="favorites__notes" v-if="item.isAction || item.isNew">
				<span class="favorites__note favorites__note_action" v-if="item.isNew">Новинка</span>
				<span class="favorites__note favorites__note_new" v-if="item.isAction">Акция</span>
			</div>
			<div class="favorites__favorite-icon">
				<img src="../../../design/assets/images/heart.png" class="favorites__favorite-active" v-if="item.favorite" @click="add">
				<img src="../../../design/assets/images/red_heart.png" class="favorites__favorite-disactive" v-if="!item.favorite" @click="add">
			</div>
			<div class="favorites__img-block">
				<img :src="item.src" class="favorites__img">
			</div>
			<div class="favorites__main-inf">
				<span class="favorites__product-name">{{ item.name }}</span>
				<span class="favorites__product-price">{{ item.cost + "Р" }}</span>
			</div>
			<div class="favorites__other-inf">
				<span class="favorites__code">Код: {{ item.code }}</span>
				<ul class="favorites__sizes-list">
					<li class="favorites__sizes-list-item" v-for="value of item.sizes">{{ value + ", " }}</li>
				</ul>
				<div class="favorites__product-colors">
					<span class="favorites__products-color_black"></span>
					<span class="favorites__products-color_red"></span>
					<span class="favorites__products-color_white"></span>
				</div>
			</div>
		</div>
	`,

	methods: {
		add: function() {
			console.log(item.favorite);
			item.favorite = !item.favorite;
			console.log(item.favorite);
		}
	}

});


var vm = new Vue({
	el: ".favorites",
	data: {
		products: [
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: true,
				isNew: true,
				actionCost: 26390,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: false,
				isNew: true,
				actionCost: 19990,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: false,
				isNew: false,
				actionCost: 26390,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: true,
				isNew: false,
				actionCost: 26390,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: false,
				isNew: true,
				actionCost: 19990,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: false,
				isNew: false,
				actionCost: 26390,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: true,
				isNew: false,
				actionCost: 26390,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: false,
				isNew: true,
				actionCost: 19990,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			},
			{
				src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
				name: "Черные ботфорты ZENUS",
				cost: 26390,
				isAction: false,
				isNew: false,
				actionCost: 26390,
				favorite: true,
				sizes: [
					35,36,37,38,39,35.5
				],
				code: "МСБ-15071",
				colors: {
					black: true,
					red: true,
					white: true
				}
			}

		]
	},
	methods: {

	},
	created: function() {

	}
})