var vm = new Vue({
	el:".main",
	data: {
		isActive:true,
		menu: {
			season: {
				isOpen: true,

				list: [
					{
						id:0, 
						text:"Летняя",
						isActive: false,
						name: "summer"
					},
					{
						id:1, 
						text:"Зимняя",
						isActive: false,
						name: "winter"
					},
					{
						id:2, 
						text:"Демисезонная",
						isActive: false,
						name: "demi-season"
					},
					{
						id:3,
						text:"Всесезонная",
						isActive: false,
						name: "all-season"
					}
				]

			},

			category: {
				isOpen: true,
				isChoosen: false,
				list: [
					{
						id: 0, 
						name: "Балетки", 
						isChoosen: true, 
						style: {
							backgroundColor: "",
							color: ""
						} ,
						seasonId: 0,
					},
					{
						id: 1, 
						name: "Босоножки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 2, 
						name: "Ботильоны",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 1,
					},
					{
						id: 3, 
						name: "Ботинки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 1,
					},
					{
						id: 4, 
						name: "Ботфорты",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 5, 
						name: "Кеды",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 6, 
						name: "Кроссовки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 7, 
						name: "Мокасины",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 1,
					},
					{
						id: 8, 
						name: "Полуботинки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 9, 
						name: "Полусапожки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 10, 
						name: "Резиновые ботинки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 11, 
						name: "Сабо",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 12, 
						name: "Сандалии",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 13, 
						name: "Сапоги",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 1,
					},
					{
						id: 14, 
						name: "Сланцы",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 15, 
						name: "Слипоны",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 3,
					},
					{
						id: 16, 
						name: "Сникерсы",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 2,
					},
					{
						id: 17, 
						name: "Тапочки",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 18, 
						name: "Топсайдеры",
						isChoosen: true,
						 style: {
						 	backgroundColor: "",
						 	color: ""
						 } ,
						 seasonId: 1,
					},
					{
						id: 19, 
						name: "Туфли",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 20, 
						name: "Шлёпанцы",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 0,
					},
					{
						id: 21, 
						name: "Эспадрильи",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 3,
					},
					{
						id: 22, 
						name: "Угги",
						 isChoosen: true,
						  style: {
						  	backgroundColor: "",
						  	color: ""
						  } ,
						  seasonId: 1,
					}
				]
			},
			brandes: {
				isOpen: true,
				isOtherOpen: false,

				autocomplete: {
					message: "",
					list: [

					]
				},

				list: [
					{
						id: 0,
						name: "Angelo Giannini",
						
					},
					{
						id: 1,
						name: "Baldinini",

					},
					{
						id: 2,
						name: "Giovanni Fabiani",

					},
					{
						id: 3,
						name: "Lancaster",

					},
					{
						id: 4,
						name: "Laura Bellariva",

					},
					{
						id: 5,
						name: "Loriblu",

					},
					{
						id: 6,
						name: "Mara",

					},
					{
						id: 7,
						name: "Voile Blanche",

					},
					{
						id: 8,
						name: "Чисто",

					},
					{
						id: 9,
						name: "От",

					},
					{
						id: 10,
						name: "Себя",

					},
					{
						id: 11,
						name: "Добавил",

					}
				]
			},
			size: {
				isOpen:true,
				list: [
					{id:0, presence:true, size: 35},
					{id:1, presence:true, size: 36},
					{id:2, presence:true, size: 37},
					{id:3, presence:true, size: 38},
					{id:4, presence:true, size: 39},
					{id:5, presence:true, size: 35.5},
					{id:6, presence:true, size: 36.5},
					{id:7, presence:true, size: 37.5},
					{id:8, presence:true, size: 38.5},
					{id:9, presence:true, size: 39.5}
				]
			},

			tematicItem: {
				heel: false
			},

			colors: {
				isOpen: true,
				list: [
					{
						id: 0,
						color: "черный",
						name: "black"

					},
					{
						id: 1,
						color: "коричневый",
						name: "brown"

					},
					{
						id: 2,
						color: "бежевый",
						name: "idk"

					},
					{
						id: 3,
						color: "белый",
						name: "white"

					},
					{
						id: 4,
						color: "Черный",
						name: "black"

					},
					{
						id: 5,
						color: "Черный",
						name: "black"

					},
					{
						id: 6,
						color: "Черный",
						name: "black"

					},
				],
			},

			catalog_blocks: [
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: true,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					],
					season: [
						"Зимняя",
						"Демисезонная"
					],
					category: "Сапоги",
					brand: "Angelo Giannini"
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: true,
					actionCost: 19990,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				},
				{
					src: "https://sateg.ru/wa-data/public/shop/products/99/01/199/images/1091/1091.970.jpg",
					name: "Черные ботфорты ZENUS",
					cost: 26390,
					isAction: false,
					isNew: false,
					actionCost: 26390,
					size: [
						35,36,37,38,39,35.5
					],
					code: "МСБ-15071",
					colors: [
						{
							color: "#936336",
							border: "936336"
						},
						{
							color: "#000",
							border: "#000"
						},
						{
							color: "#fff",
							border: "#000"
						}
					]
				}
			],

			catalog: {
				popularFilter: true,
				newFilter: false,
				expensiveFilter: false,
				cheepFilter: false
			},

			quantity: 256,

			instagram: {
				url: "https://i.pinimg.com/474x/11/94/9a/11949add2687e05b22c3d8256628e708.jpg",
				likes: 162,
				comments: 25
			},
			hashtages: [
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки",
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки",
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки",
				"Итальянская обувь маленьких размеров",
				"Итальянские женские кроссовки"
			]
			
		}
		
	},

	computed: {
		filteredList: function() {
			var mes = this.menu.brandes.autocomplete.message;
			return this.menu.brandes.list.filter(function (elem) {
				if (mes === '') return true;
				else return elem.name.indexOf(mes) > -1;
			})
		}
	},


	methods: {


		filter: function(event) {
			console.log(event.target);
		},


		choozeCategory: function() {
			this.menu.category.list[event.target.dataset.id].style.backgroundColor = "#b6814f";
			this.menu.category.list[event.target.dataset.id].style.color = "#fff";
			this.menu.category.isChoosen = true;
			for (item of this.menu.category.list) {
				if (item.id != event.target.dataset.id) {
					item.isChoosen = false;
				}
			}
			switch (event.target.dataset.id) {
				case "3": {this.menu.tematicItem.heel = true;};break;
			}
		},

		showOtherCategory: function() {
			
			for (item of this.menu.category.list) {
					item.isChoosen = true;
					item.style.backgroundColor = "";
					item.style.color = "";
			}

			for (item in this.menu.tematicItem) {
				this.menu.tematicItem[item] = false;
			}
		},

		openBrandesSearch: function() {
			this.menu.brandes.isOtherOpen = true;
		}

	},

	// watch: {
	// 	message: function(val) {
	// 		console.log(5);
	// 	}
	// },


	created: function() {

	}
})