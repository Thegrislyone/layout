$( function() {
    $( ".slider-cost-range" ).slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ 3400, 22800 ],
      slide: function( event, ui ) {
        $(".catalog__first-cost-sum" ).val(ui.values[ 0 ]);
        $(".catalog__second-cost-sum" ).val(ui.values[ 1 ]);
      }
    });
    $(".catalog__first-cost-sum" ).val($( ".slider-cost-range" ).slider( "values", 0 ));
    $(".catalog__second-cost-sum" ).val($( ".slider-cost-range" ).slider( "values", 1 ));




  });

$( function() {
$( ".slider-high-range" ).slider({
      range: true,
      min: 0,
      max: 15,
      values: [ 3, 12 ],
      slide: function( event, ui ) {
        $(".catalog__high-first-sum" ).val(ui.values[ 0 ]);
        $(".catalog__high-second-sum" ).val(ui.values[ 1 ]);
      }
    });
    $(".catalog__high-first-sum" ).val($( ".slider-high-range" ).slider( "values", 0 ));
    $(".catalog__high-second-sum" ).val($( ".slider-high-range" ).slider( "values", 1 ));



  });



$(document).ready(function() {
  $(".ui-slider-handle").mousedown(() => {
    $("body").css('cursor', 'grabbing');
  });
  $("body").mouseup(() => {
    $("body").css('cursor', 'default');
  });
})